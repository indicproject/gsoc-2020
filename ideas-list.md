Ideas for Google Summer of Code 2020
===================================

If you want to propose an idea, please write it to contact@indicproject.org or in GSoC Mailing list . [[Join list](https://groups.google.com/a/indicproject.org/forum/#!forum/gsoc)] 

Matrix Room - [#indicproject:matrix.org](https://matrix.to/#/#indicproject:matrix.org)

Telegram - [@smc_project](https://t.me/smc_project)

# Indic Keyboard

## Indic Keyboard SDK

There are several thirdparty developers interested in packing Indic Keyboard along with their app, so that they don't need to worry about end user having installed an input method.


**Complexity** - Difficult

**Confirmed Mentors(s)** - Jishnu Mohan

**How to contact the mentor** - jishnu7 on IRC

**Expertise required** - Java, Android Development

**Related Links**
 - [Project Page](https://gitlab.com/indicproject/indic-keyboard)

## Bluetooth/USB Keyboard support

As title suggests, indic Keyboard currently supports only virtual keyboards.
This task is to support physical keyboards.


**Complexity** - Medium

**Confirmed Mentors(s)** - Jishnu Mohan

**How to contact the mentor** - jishnu7 on #indicproject (irc.freenode.net)

**Expertise required** - Java, Android Development

**Related Links** 
 - [Project Page](https://gitlab.com/indicproject/indic-keyboard)

# Varnam Project

## Make the learnings file common for all schemes under a language

Assume there are 2 schemes for a language. `langcode-phonetic` and `langcode-inscript`. Today, Varnam needs to learn all the words separately for each scheme. Ideally, we should be able to reuse the learnings for a language across different schemes.

This involves changing the learning system, and transliteration system in Varnam. For more details, visit this [issue](https://github.com/varnamproject/libvarnam/issues/141) on `libvarnam`.

Description

**Complexity** - Difficult

**Confirmed Mentors(s)** - Navaneeth K N

**How to contact the mentor** - navaneethkn at gmail

**Expertise required** - C, algorithms, databases

**Related Links**
 - [Website](https://www.varnamproject.com)
 - [Source code](https://github.com/varnamproject)
 - [Issue on libvarnam](https://github.com/varnamproject/libvarnam/issues/141)
 


# Libindic

## Port LibIndic to Python3 and adapt REST API to match it

Many modules in LibIndic currently doesn't have Python 3 support. Since it has been officially announced that Python 2 will be deprecated soon, it is better to update the LibIndic codebase to Python 3. This also involves proper namespacing of packages, which is currently in a partial stage. So, this idea involves three steps
 * Port code to support Python 3.
 * Properly namespace the packages
 * Update the REST API to consider these changes

**Complexity** - Easy

**Confirmed Mentors(s)** - Balasankar C

**How to contact the mentor** - balasankarc AT autistici DOT org

**Expertise required** - Python, Concepts of REST.

**Related Links** 
 - [LibIndic Projects](http://github.com/libindic/)
 - [PEP 420 - Namespace Packages](https://www.python.org/dev/peps/pep-0420/)

## Improve Sandhi Splitter

Sandhi-Splitter currenty uses a Naive Bayesian strategy. Explore the possibilities of using RNN (Recurrent Neural Network) strategies like LSTM (Long Short Term Memory) as an alternative.

**Complexity** - Intermediate

**Confirmed Mentors(s)** -

**How to contact the mentor** -

**Expertise required** - Python, AI - specifically Neural Network concepts.

**Related Links** 
 - [Sandhi Splitter module](https://github.com/libindic/sandhi-splitter)
 -

# Indicjs

## Porting libindic to javascript

Javascript modules that can run on both server and client will let wider adoption of indic language tools on the web. The idea is to port existing libindic modules from python to javascript (preferably ES6) and have a suite of such modules.

There is a considerable amount of people who understands an Indic language, but can't read it. Transliteration can help them to read the text better than translation. Porting libindic's transliteration module to JavaScript and making a web extension to transliterate Indic language websites will benefit such people. This Javascript module can then be used in Android or any other platforms to implement Indic language transliteration and similar functionality.

**Complexity** - Easy to Medium

**Confirmed Mentors(s)** - [Akshay S Dinesh](http://asd.learnlearn.in)

**How to contact the mentor** - ping asdofindia in #indicproject

**Expertise required** - javascript, ux

**Related Links**
 - [indicjs prototype](https://gitlab.com/indicproject/indicjs)
 - [libindic transliteration demo](https://libindic.org/Transliteration)
 - [A reddit Malayalam transliteration bot](https://www.reddit.com/r/Kerala/comments/8n6xy2/presenting_uvincent_gomez_the_mlen/)
 - [A Twitter Malayalam transliteration bot](https://twitter.com/ManglishBot)

## Automatic creation of ascii unicode conversion maps from ttf files of ascii fonts

Character recognition is good enough to allow computers detect characters from standard fonts. We can use this to generate the ascii-unicode conversion map for any given TTF thereby eliminating the need for manual conversion of new fonts.

**Complexity** - Easy to Medium

**Confirmed Mentors(s)** - [Akshay S Dinesh](http://asd.learnlearn.in)

**How to contact the mentor** - ping asdofindia in #indicproject

**Expertise required** - OCR algorithms, font standards

**Related Links** 
 - [opentype.js](https://github.com/nodebox/opentype.js)
 - [Sketchy.js](https://github.com/kjkjava/Sketchy.js)

# Indic Compatability

## Openstreetmap Indic Localization helper tool 

Currently, OSM localisations are done manually by volunteer editors manually mostly. Some editors have built custom tools to bulk upload localizations, but there is no easy tool which could be easily used to localize manually / verify machine-assisted localizations to improve the speed of localization in OSM map data.

Nomino[1] is a tool available to perform localization, but the tool is unmaintained, doesnt offer suggestions. With Wikidata and google translate API's we can reasonably ensure correctness of automated translation if they match, when they dont match, it still provides reasonably good suggestions, which users can just pick. Since we are dealing with names of places, copyright issues dont arise. A tool, like Nomino, with suggestions from Wikidata, Google translate integrating with OSM APIs would be of great utility in increasing the pace of localizations, while ensuring that such localizations are manually verified by user and not purely machine translated.


**Complexity** - Medium

**Confirmed Mentors(s)** - Srikanth Lakshman 

**How to contact the mentor** -  logic on  #indicproject (irc.freenode.net)

**Expertise required** - js and any backend language, preferably Python

**Related Links** 

 - [nomino](http://nomino.openstreetmap.fr/)
 - [Indic in Open streetmap](https://www.openstreetmap.org/user/PlaneMad/diary/38176)


